<?php

function mailchimp_interface_default_rules_configuration(){
  $items = array();
  $items['rules_enviar_boletin'] = entity_import('rules_config', '{ "rules_enviar_boletin" : {
        "LABEL" : "enviar boletin",
        "PLUGIN" : "reaction rule",
        "OWNER" : "rules",
        "REQUIRES" : [ "php", "rules" ],
        "ON" : {
          "node_insert--boletin" : { "bundle" : "boletin" },
          "node_update--boletin" : { "bundle" : "boletin" }
         },
        "DO" : [
          { "php_eval" : { "code" : "mailchimp_interface_create_campaing_and_email($node);" } }
         ]
        }
    }');
return $items;
}
?>
