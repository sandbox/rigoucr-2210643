<?php

/**
 * @file mailchimp_interface.admin.inc
 * MailChimp Integration.
 * @author: Rodrigo Espinoza Brenes <espinoza.brenes.rodrigo@gmail.com>
 * @author: Edwin Torrez <edwtorba@edwtorba.com>
 * @version: 1.0
 */


/**
 * Define mailchimp settings form
 */
function mailchimp_interface_admin_form($form, &$form_state) {

  $form = array();

  $form['mailchimp_interface_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#description' => t('You can get it from Mailchimp site.'),
    '#default_value' => variable_get('mailchimp_interface_api_key', ''),
    '#required' => TRUE,
  );


  $form['mailchimp_interface_address_list'] = array(
    '#type' => 'textfield',
    '#title' => t('Address list'),
    '#description' => t('You can get it from Mailchimp site in lists configuration.'),
    '#default_value' => variable_get('mailchimp_interface_address_list', ''),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
