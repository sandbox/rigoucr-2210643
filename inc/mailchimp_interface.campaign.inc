<?php

/**
 * @file mailchimp_interface.campaign.inc
 * MailChimp Integration.
 * @author: Rodrigo Espinoza Brenes <espinoza.brenes.rodrigo@gmail.com>
 * @version: 1.0
 */


/**
 * Define mailchimp settings form
 */
function mailchimp_interface_settings_form($form, &$form_state) {

  if(variable_get('mailchimp_interface_address_list','') != '' && variable_get('mailchimp_interface_api_key','') != ''){

    $form['mailchimp_interface_user_register_form_option'] = array(
      '#type' => 'select',
      '#title' => t('Do you want a subscription option in user registration form?'),
      '#description' => t('This allows you to add an option in user registration form to automatically subscribe to newsletter.'),
      '#options' => array(
        1 => t('Yes'),
        0 => t('No'),
      ),
      '#default_value' => variable_get('mailchimp_interface_user_register_form_option', 0),
      '#required' => TRUE,
    );

    $form['mailchimp_interface_contact_site_form_option'] = array(
      '#type' => 'select',
      '#title' => t('Do you want a subscription option in contact form?'),
      '#description' => t('This allows you to add an option in contact form to automatically subscribe to newsletter.'),
      '#options' => array(
        1 => t('Yes'),
        0 => t('No'),
      ),
      '#default_value' => variable_get('mailchimp_interface_contact_site_form_option', 0),
      '#required' => TRUE,
    );

    $options_types = array();
    $entity_info = entity_get_info('node');
    $view_modes = (array_keys($entity_info['view modes']));

    foreach ($view_modes as $value) {
      $options_types[$value] = $value;
    }

    $form['mailchimp_interface_view_mode'] = array(
      '#type' => 'select',
      '#title' => t('View mode for Newsletter'),
      '#description' => t('The view mode you choose here will be used for displaying the email contents.'),
      '#options' => $options_types,
      '#default_value' => variable_get('mailchimp_interface_view_mode', ''),
      '#required' => TRUE,
    );

    $form['mailchimp_interface_template'] = array(
      '#type' => 'select',
      '#title' => t('Email Template'),
      '#description' => t('This should be previosly defined in Mailchimp website. In templates folder in this module you will find an example.'),
      '#options' => mailchimp_interface_get_templates_list(),
      '#multiple' => FALSE,
      '#default_value' => variable_get('mailchimp_interface_template', ''),
      '#required' => TRUE,
    );
    return system_settings_form($form);
  }else{

    drupal_set_message(t('You must set the apiKey and Email List ID'),'warning');
  }
}

/**
 *Se obtiene la lista de los templates disponibles 
 */
function mailchimp_interface_get_templates_list(){

  require_once(drupal_get_path('module', 'mailchimp_interface') . '/libs/MailChimp.class.php');
  $MailChimp = new MailChimp(variable_get('mailchimp_interface_api_key', ''));

  $parameters = array(
    'apiKey'=> variable_get('mailchimp_interface_api_key'),
    'types' => array(
      'user'=>true,
    ),
  );

  $templates = array();
  $opciones = array();
  $templates = $MailChimp->call('templates/list', $parameters);


  foreach ($templates['user'] as $item){
    $opciones[$item['id']]  = $item['name'];
  }
  if(!count($opciones)){
  drupal_set_message(t('This should be previosly defined in Mailchimp website. In templates folder in this module you will find an example.'), 'warning');
      }
  return $opciones;

}
